var path = require('path');
var express = require('express');
var app = express();
var PORT = 8080;

/**
 * @type {Array}
 */
var pages = require('./server-config/route');
/**
 * @type {Array}
 */
var staticPaths = require('./server-config/static');
/**
 * @type {Array}
 */
var modules = require('./server-config/modules');

console.log('> Starting dev server...');

staticPaths.forEach(function (sPath){
  var staticPath = path.posix.join(path.resolve(__dirname, './'), sPath);
  app.use('/' + sPath, express.static(staticPath));
});

pages.forEach(function (page){
  app.get(page.url, function(req, res) {
    res.sendFile(path.join(path.resolve(__dirname, './templates/', page.html)));
  });
});

modules.forEach(function (module){
  app.get('/' + module.url, function(req, res) {
    res.sendFile(path.join(path.resolve(__dirname, './', module.path)));
  });
});

server = app.listen(PORT, function() {
  var uri = 'http://localhost:' + PORT;
  console.log('> Server started on ' + uri)
});