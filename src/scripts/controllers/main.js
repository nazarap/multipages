/**
 * Created by norysc on 04.05.18.
 */
require(['../module'], function (module) {
  require(['basic/dom', 'basic/scope'], function (dom, scope) {
    scope.data.name0 = 'Nazarco';
    scope.data.name1 = null;

    var e = dom.element;
    var testList = [
      {
        title: 'Title 1',
        value: 123
      },
      {
        title: 'Title 2',
        value: 'helloworld'
      }
    ];

    var nodeList = testList.map(function (item, index) {
      return dom.append(
        dom.create(e('div', {})),
        [
          e(
            'button',
            {
              id: 'button' + index,
              innerHTML: item.title,
              className: 'test',
              show: 'name' + index,
              click: (function (index) {console.log('name' + index)}).bind(this, index)
            }
          ),
          e(
            'input',
            {
              id: 'input' + index,
              value: item.value,
              className: 'test 12',
              model: 'name' + index,
              change: function () {
                console.log('i am here !!')
              }
            }
          )
        ])
    });
    dom.append(
        'test1'
      , nodeList
    );

    dom.bind.call(scope, 'test0', 'name1');

  });
});