/**
 * Created by norysc on 04.05.18.
 */

/**
 * Create config for requirejs
 */
requirejs.config({
  baseUrl: 'src/scripts',
  paths: {
    axios: '/node_modules/axios/dist/axios'
  }
});

/**
 * @module module - main module for application pages
 */
define(['basic/api'], function (Api) {
  return {
    Api: Api
  }
});