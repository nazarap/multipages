/**
 * Created by norysc on 04.05.18.
 */

/**
 * @module dom - create function for working with html dom
 */
define(['./dom-types', './scope'], function (types, scope) {
  var elements = [];
  var Element = types.Element;
  var Attributes = types.Attributes;
  var actions = {};

  /**
   * @private
   * @function _generateId - generate id for element without id
   *
   * @return {String}
   */
  var _generateId = function () {
    return Math.random().toString(36).substring(7);
  };

  /**
   * @private
   * @function _getElement - get element by id
   *
   * @return {HTMLElement}
   */
  var _getElement = function (id) {
    if (elements[id]) return elements[id];
    return document.getElementById(id);
  };

  /**
   * @private
   * @function _addElement - Add element to elements list
   *
   * @param {HTMLElement|String} element - node id or node
   */
  var _addElement = function (element) {
    if (_checkElement(element)) {
      if (!element.id) return;
      elements[element.id] = element;
    } else {
      elements[element] = _getElement(element);
    }
  };

  /**
   * @private
   * @function _checkElement - Check if element type if HTMLElement
   *
   * @param element - variable what we can check on implemented HTMLElement
   *
   * @return {Boolean}
   */
  var _checkElement = function (element) {
    return element instanceof HTMLElement;
  };

  /**
   * @private
   * @function _getNode - Return element if it`s implemented HTMLElement or return node by element id
   *
   * @param {HTMLElement|String} element - node id or node
   *
   * @return {HTMLElement}
   */
  var _getNode = function (element) {
    if (!_checkElement(element)) {
      element = _getElement(element);
    }
    return element;
  };

  /**
   * @private
   * @function _createElement - Create element
   *
   * @param {Element} element
   * @param {String|HTMLElement|null} parent
   * @return {HTMLElement}
   */
  var _createElement = function (element, parent) {
    if (element.attr.id && _getElement(element.attr.id))
      console.error('Duplicate element with id ' + element.attr.id);

    element.attr.id = element.attr.id || _generateId();
    var newElement = document.createElement(element.type);
    Object.keys(element.attr).forEach(function (attrName) {
      if (!element.attr[attrName]) return;
      if (actions[attrName]) {
        actions[attrName](newElement, element.attr[attrName], parent);
        return;
      }
      newElement[attrName] = element.attr[attrName];
    });
    return newElement;
  };

  /**
   * @private
   * @function _attacheToParent - Put {child} into {parent}
   *
   * @param {HTMLElement|String} parent
   * @param {HTMLElement} child
   *
   * @return {HTMLElement|undefined} - parent node
   */
  var _attacheToParent = function (parent, child) {
    if (!parent) return;

    if (!_checkElement(parent)) {
      parent = _getElement(parent)
    }
    _addElement(parent);
    parent.appendChild(child);

    return parent;
  };

  /**
   * @private
   * @function _createChildForParent - Create child and attache to parent
   *
   * @param {String|HTMLElement} parent
   * @param {Element|HTMLElement} child
   *
   * @return {HTMLElement} - child node
   */
  var _createChildForParent = function (parent, child) {
    if (!_checkElement(child)){
      child = _createElement(child, parent)
    }
    _addElement(child);
    _attacheToParent(parent, child);

    return child;
  };

  /**
   * @function create - Create node
   *
   * @param {Element|HTMLElement} element
   *
   * @return {HTMLElement}
   */
  var create = function (element) {
    if (!_checkElement(element)){
      element = _createElement(element, null);
    }
    return element;
  };

  /**
   * @function append - Put {child[s]} into {parent} node
   *
   * @param {String|HTMLElement} parent
   * @param {Element|HTMLElement|Array} child
   *
   * @return {HTMLElement} - parent node
   */
  var append = function (parent, child) {
    if (child instanceof Array) {
      child.forEach(function (childItem) {
        _createChildForParent(parent, childItem);
      })
    } else {
      _createChildForParent(parent, child);
    }

    return parent;
  };

  /**
   * @function element - Create model for htmlElement
   *
   * @param {String} type
   * @param {Object} attr
   *
   * @return {Element}
   */
  var element = function (type, attr) {
    return new Element(type, new Attributes(attr));
  };

  /**
   * @function actions.click - Attache click action for node
   *
   * @param {HTMLElement|String} element - node id or node
   * @param {Function} callback
   */
  actions.click = function (element, callback) {
    element = _getNode(element);
    _addElement(element);
    element.onclick = callback;
  };

  /**
   * @function actions.change - Attache change action for node
   *
   * @param {HTMLElement|String} element - node id or node
   * @param {Function} callback
   */
  actions.change = function (element, callback) {
    element = _getNode(element);
    _addElement(element);
    element.onchange = function (event) {
      callback(event.target.value);
    };
  };

  /**
   * @function actions.input - Attache input action for node
   *
   * @param {HTMLElement|String} element - node id or node
   * @param {Function} callback
   */
  actions.input = function (element, callback) {
    element = _getNode(element);
    _addElement(element);
    element.oninput = function (event) {
      callback(event.target.value);
    };
  };

  /**
   * @function actions.model - Create two way binding model attr for [input, textarea]
   *
   * @param {HTMLElement|String} element - node element for attach model attr
   * @param {String} propName - prop name from scope
   */
  actions.model = function (element, propName) {
    element = _getNode(element);
    element.value = scope.data[propName] || '';
    actions.input(element, function (newValue) {
      scope.data[propName] = newValue;
    });
    scope.watch(propName, function (oldValue, newValue) {
      element.value = newValue;
    })
  };

  /**
   * @function actions.bind - Create one way binding bind attr for [div, h1-h6, ...].
   * Replace all inner HTML in element
   *
   * @param {HTMLElement|String} element - node element for attach model attr or node id
   * @param {String} propName - prop name from scope
   */
  actions.bind = function (element, propName) {
    element = _getNode(element);
    element.textContent = scope.data[propName];
    scope.watch(propName, function (oldValue, newValue) {
      element.textContent = newValue;
    })
  };

  /**
   * @function actions.show - Remove and return element by propName
   *
   * @param {HTMLElement|String} element - node element for attach model attr or node id
   * @param {String} propName - prop name from scope
   * @param {HTMLElement} parent - element parent node
   */
  actions.show = function (element, propName, parent) {
    if (!_checkElement(parent)) {
      console.error('Attribute \'show\' is available only for node with parent node');
      return;
    }
    element = _getNode(element);
    var comment = document.createComment("Removed element");
    var isShow = false;
    scope.watch(propName, function (oldValue, newValue) {
      if (newValue && isShow) {
        isShow = false;
        elements[parent.id].replaceChild(elements[element.id], comment);
      } else if (!newValue && !isShow) {
        isShow = true;
        elements[parent.id].replaceChild(comment, elements[element.id]);
      }
    })
  };

  return {
      element: element
    , append: append
    , create: create
    , click: actions.click
    , change: actions.change
    , input: actions.input
    , model: actions.model
    , bind: actions.bind
    , show: actions.show
  };
});