/**
 * Created by norysc on 04.05.18.
 */

/**
 * @module api - main api module
 */
define(function (require) {
  var Axios = require('axios');
  var Api = Axios.create();

  return {
    authors: function () {
      return Api.get('http://localhost:8080');
    }
  };
});