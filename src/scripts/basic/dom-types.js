/**
 * Created by norysc on 05.05.18.
 */

/**
 * @module dom-types - element types list
 */
define(function () {
  /**
   * Class Element - information about node
   *
   * @param {String} type
   * @param {Attributes} attributes
   */
  var Element = function (type, attributes) {
    this.type = type;
    this.attr = attributes;
  };

  /**
   * Class Attributes - information about node attributes
   *
   * @param {Object} attr
   */
  var Attributes = function (attr) {
    this.id = attr.id;
    this.innerHTML = attr.innerHTML || '';
    this.className = attr.className || '';
    this.value = attr.value || '';
    this.click = attr.click;
    this.change = attr.change;
    this.model = attr.model;
    this.bind = attr.bind;
    this.show = attr.show;
  };

  return {
      Element: Element
    , Attributes: Attributes
  };
});