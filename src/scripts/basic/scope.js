/**
 * Created by norysc on 05.05.18.
 */

/**
 * @module scope - scope for module with data and methods
 */
define(function () {
  var data = {};
  var methods = {};
  var _watchers = {};
  defineWatchFunction(data);

  /**
   * @function watch - create watcher for data
   *
   * @param {String} dataName
   * @param {Function} callback
   */
  var watch = function (dataName, callback) {
    if (!_watchers[dataName]) {
      _watchers[dataName] = [];
      data.watch(dataName, function (id, oldValue, newValue) {
        _watchers[id].forEach(function (callback) {
          callback(oldValue, newValue);
        });
        return newValue;
      });
    }
    _watchers[dataName].push(callback);
  };

  return {
      data: data
    , methods: methods
    , watch: watch
  };
});

function defineWatchFunction(obj) {
  if (!obj.watch) {
    Object.defineProperty(obj, "watch", {
      enumerable: false
      , configurable: true
      , writable: false
      , value: function (prop, handler) {
        var
          oldval = this[prop]
          , newval = oldval
          , getter = function () {
            return newval;
          }
          , setter = function (val) {
            if (oldval === val) return val;
            oldval = val;
            return newval = handler.call(this, prop, oldval, val);
          }
        ;

        if (delete this[prop]) { // can't watch constants
          Object.defineProperty(this, prop, {
            get: getter
            , set: setter
            , enumerable: true
            , configurable: true
          });
        }
      }
    });
  }

  // object.unwatch
  if (!obj.unwatch) {
    Object.defineProperty(obj, "unwatch", {
      enumerable: false
      , configurable: true
      , writable: false
      , value: function (prop) {
        var val = this[prop];
        delete this[prop]; // remove accessors
        this[prop] = val;
      }
    });
  }
}